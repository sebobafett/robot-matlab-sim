%%%% Simulation Parameters %%%%%%%

% Vehicle Parameters
 lead_pos = [0, 0];      % starting position of leader
 follow_pos = [-3, -2];   % starting position of follower
 lead_vel = [1, 0];       % velocity of leader
 follow_vel = [1.5, 0];  % velocity of follower     
 pos_delta = [-0.5, 0.5];  % target distance behind leader

% Simulation Parameters          
dt = 0.03;             % time step
duration = 3;         % sim duration
kp = 10;                % proportional gain
kd = 0;                  % derivative gain
max_speed = 3;   % max speed of follower

%%%%%%%%%%%%%%%%%%%%%%%%

sim = Robots(lead_pos, follow_pos, lead_vel, follow_vel, pos_delta);
sim.Simulate(dt, duration, kp, kd, max_speed);
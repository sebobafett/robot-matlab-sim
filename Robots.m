classdef Robots < handle
    properties
        lead_pos
        follow_pos
        lead_vel
        follow_vel
        pos_delta
    end
    
    methods
        function obj = Robots(lead_pos, follow_pos, lead_vel, follow_vel, pos_delta)
            obj.lead_pos = lead_pos;
            obj.follow_pos = follow_pos; 
            obj.lead_vel = lead_vel;
            obj.follow_vel = follow_vel;
            obj.pos_delta = pos_delta;
        end
        
        function Simulate(obj, dt, tf, kp, kd, max_speed) 
            figure();
            axis([-5,5,-5, 5]);
           
            for time = 0:dt:tf 
                % Update robot state
                e =obj.UpdateState(dt, kp, kd, max_speed);
                obj.PlotSystem(time, e);
                pause(dt/10);
            end
        end
        
        function PlotSystem(obj, time, e)  
            % Plots each robot
            subplot(2,2,[1, 2]);
            plot(obj.lead_pos(1), obj.lead_pos(2), 'b*', ...
                   obj.follow_pos(1), obj.follow_pos(2), 'ro', ...
                   obj.lead_pos(1) + obj.pos_delta(1), obj.lead_pos(2) + obj.pos_delta(2), 'k.');
            axis([obj.follow_pos(1) - 2, obj.follow_pos(1) + 2, obj.follow_pos(2) - 2, obj.follow_pos(2) + 2]);
            %legend({'Leader', 'Follower', 'Target'},'Location', 'northwest', 'NumColumns', 3)
            title('Simulation')
            grid on;
  
            % Plot error and vels
            subplot(2,2,3);
            hold on;
            speed = norm(obj.follow_vel);
            plot(time, speed, 'b.');
            title('Follower Speed')
            xlabel('Time [s]')
            ylabel('Speed [m/s]')
            grid on;
            
            subplot(2,2,4);
            e = norm(e);
            plot(time, e, 'b.');
            hold on;
            title('Follower Error')
            xlabel('Time [s]')
            ylabel('Distance [m]')
            grid on;
        end
               
        function e = UpdateState(obj, dt, kp, kd, max_speed)
            % Control      
            e = obj.lead_pos - obj.follow_pos + obj.pos_delta;
            e_dot = obj.lead_vel - obj.follow_vel;

            v = e * kp + e_dot * kd + obj.lead_vel; % With feed forward

            obj.follow_vel = obj.limit_speed(v, max_speed);
                        
            % Kinematics
            obj.lead_pos = obj.lead_pos + obj.lead_vel*dt;
            obj.follow_pos = obj.follow_pos + obj.follow_vel*dt;
        end
        
        function v = limit_speed(~, v, max_speed)
            % function to keep the follow speed reasonable
            norm_v = norm(v);
            max_speed = min(max_speed, norm_v);
            v = v / norm_v * max_speed;
        end
    end
end